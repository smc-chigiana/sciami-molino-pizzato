# Sciami Molino `puredata` patch

These are instructions concerning the use of this `puredata` patch. Feel free
to add to it if you are so inclined.

## Usage

### Start

The best way to run this patch is to run `puredata` from a terminal emulator
(command line). The line is:

```sh
$ pd -prefsfile config main.pd
```

This will load all the preferences, paths, etc.

If you don't want to use the command line, please make sure that the `config`
file gets loaded via the args by going in the `file` menu -> `preferences` ->
`Startup...` -> `startup flags` and set there `-prefsfile config` (where
`config` might need the full path name).

### Jack configuration

As configured, this patch outputs sound over 11 channels (1-8 octophonic, 9-10
stereo dump, 11 mono reverb send). If you want to hear these channels through,
say, your stereo headphones you have to connect the outlets to your left-right
system sinks.
