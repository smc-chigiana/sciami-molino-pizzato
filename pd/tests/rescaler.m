M = 252;
invM = 1/M;
k = [0:M-1]; % numero di voce
n = k+1;
x = [0:invM:1-invM];
y = 1 - (x./((M-k).*n));
%
plot(x, y);
% axis([0 1 0 1.1])
